#include <stdio.h>
#include <string.h>

int main() {
	
	int senLen, strLen;
	char* flag;
	
	while( strcmp( flag, "n" ) ) {
		// コメント部分全体の桁数を入力
		printf( "the length of \"/***...***/\":" );
		scanf( "%d", &senLen );
		
		// Centeringしたい部分の桁数を入力
		printf( "the length of inner String:" );
		scanf( "%d", &strLen );
		
		// 演出
		printf( "Calculating...\n" );
		
		// 書き始めるべき桁数を表示
		printf( "Okay, you should put the String on line %d.\n\n", senLen / 2 - strLen / 2 );
		
		// 適当な文字を入力させて終了。これが無いとコンソールがすぐ消えてしまう。
		printf( "Do you want to continue?( y or n )" );
		scanf( "%s", flag );
	}
	
	return 0;

}